# nftlb PKGBUILD

PKGBUILD to package up [nftlb](https://github.com/zevenet/nftlb).

## Usage
* Build and install the package `makepkg -si`
* Set the API key and desired port that will be used to connect within `/etc/nftlb.conf`
* Start the service `sudo systemctl --now enable nftlib.service`

## Author

Zarren Spry <zarrenspry@gmail.com>

